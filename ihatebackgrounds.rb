require 'RMagick'
require 'pry'



def trans_mask_sobel(image)
  # select image
  image = select_image(image)
  image = fill_background(image)
  image.write("trans_mask_sobel.png")
  return image
end


def select_image(image)

  image.write("begin_selected_image.png")
  
  # negate image
  image = image.negate()
  image.write("negated_image.png")
  puts "negated image"
  
  # edge image using sobel filter
  image = image.edge(2)
  image.write("edged_image.png")
  puts "edged image"
  
  # blur image
  image = image.gaussian_blur(2, 5)
  image.write("blurred_image.png")
  puts "blurred image"
  
  # threshold image
  image = image.threshold(24)
  image.write("threshold.png")
  puts "thresholded image"
  
  # adaptive threshold
  image = image.adaptive_threshold(5, 5, 5)
  image.write("adaptive.png")
  puts "local adaptive threshold image"
  
  image.write("selected_image.png")
  
  return image
end


def alpha_composite(image, mask)
  # composite two images together by overriding one opacity channel

  compos = mask.composite(image, image.rows, image.columns, Magick::CopyOpacityCompositeOp)
  image.write("composite_image.png")
  puts "composited images"
  return compos
end


def fill_background(image)
  # image  = image.fill('magenta')
  width, height = [image.columns, image.rows]
  image = image.color_floodfill(0, 0, 'magenta')
  image = image.color_floodfill(width-1, 0, 'magenta')
  image = image.color_floodfill(0, height-1, 'magenta')
  image = image.transparent('magenta')
  image.write("filled_background.png")
  puts "filled background"
  return image
end


def remove_background(image_file)
  image = Magick::Image.read(image_file).first
  transmask = trans_mask_sobel(image)
 
  image = alpha_composite(transmask, image)
#  image.trim
  image.write("output.png")
  puts "finished"
end



image_file = "test.jpg"
remove_background(image_file)
